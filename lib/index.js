// Load modules
var Hapi= require('hapi');
var Hoek = require('hoek');
var jwt= require('jsonwebtoken');
var Boom=require('boom');

// Declare internals

var internals = {};


exports.register = function (plugin, options, next) {

    plugin.auth.scheme('token', internals.implementation);

    next();
};


internals.implementation = function (server, options) {
    
    Hoek.assert(options, 'Missing token auth strategy options');
    Hoek.assert(typeof options.secret === 'string', 'options.secret must be a string in token scheme');

    var settings = Hoek.clone(options);

    var scheme = {
        authenticate: function (request, reply) {

            var req = request.raw.req;
            var authorization = req.headers.authorization;
            if (!authorization) {
                return reply(Boom.unauthorized(null, 'Token'));
            }

            // #for udesk testing
            // var parts = authorization.split(' ');
            // if (parts[0] &&
            //     parts[0].toLowerCase() !== 'bearer') {

            //     return reply(Boom.unauthorized(null, 'Token'));
            // }

            // if (parts.length !== 2) {
            //     return reply(Boom.badRequest('Bad HTTP authentication header format', 'Token'));
            // }


             jwt.verify(authorization,settings.secret,function(err,data){
                credentials = data || null;
                 if(err) return reply(Boom.unauthorized('Bad username or password', 'Token'), { credentials: credentials });
                 if(data&&data.id) return reply.continue({ credentials: credentials }); //authenticated
                 return reply(Boom.unauthorized('Bad token, please login again', 'Token'),{credentials:null});

             });

        }
    };

    return scheme;
};



exports.register.attributes = {
    pkg: require('../package.json')
};
